# OpenStore Infrastructure

Infrastructure setup scripts for the [OpenStore](https://open-store.io/) using ansible.

## Commands

- Install ansible: `pip3 install --user ansible`
- Install dependencies: `ansible-galaxy install -r requirements.yml --force`
- Make sure all hosts are accessible: `ansible all -i hosts -m ping -u <user>`
- Run the setup process on a host: `ansible-playbook -i hosts setup.yml -u <user> -l <group> --ask-become-pass`

## License

Copyright (C) 2023 [Brian Douglass](http://bhdouglass.com/)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
